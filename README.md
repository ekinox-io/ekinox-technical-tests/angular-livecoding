# Technical Test

## Enoncé

Le SimonShop est un boutique de vente de dvd en ligne. Notre collection contient deux types de DVD :
- Des DVD appartenant à des sagas mythiques du cinéma (Star Wars ou Retour vers le futur)
- D'autres DVD se suffisant à eux même et n'appartenant à aucune saga (La petite sirène, Karate Kid, et bien d'autres)

Dans le cadre d'une campagne promotionnelle pour la saga Back to the future, votre tâche est d'aider SimonShop à valider le panier de l'utilisateur. 

### Affichage et fonctionnalites d'ajout au panier

Dans un premier temps, vous devrez utiliser le service `CartService`, injecté dans `CartComponent`, pour afficher la liste des DVD disponibles.

Vous devrez egalement vous assurer de la bonne fonctionnalite d'ajout et de retrait au panier

### Calcul du prix 

Le calcul du prix est le suivant :

- Le DVD d'un volet hors saga vaut 20 €
- Le DVD d'un volet de n'importe quelle saga vaut 15 €
- Pour l'achat de 2 volets DIFFÉRENTS de la saga `Back to the future`, on applique une réduction de 10 % sur l'ensemble des DVDs de la saga `Back to the future` achetés
- Pour l'achat de 3 volets DIFFÉRENTS de la saga `Back to the future`, on applique une réduction de 20 % sur l'ensemble des DVDs de la saga `Back to the future` achetés

## Quelques exemples de calcul de prix

**Exemple n°1**

```
Panier :

- Back to the Future 1
- Star wars episode 1
- La petite sirene

Prix total : 50
```

**Exemple n°2**

```
Panier :

- Back to the Future 1
- Back to the Future 2
- Back to the Future 3

Prix total : 36
```

**Exemple n°3**

```
Panier :

- Back to the Future 1
- Back to the Future 3

Prix total : 27
```

**Exemple n°4 :**

```
Panier :

- Back to the Future 1

Prix total : 15
```

**Exemple n°5 :**

```
Panier :

- Back to the Future 1
- Back to the Future 2
- Back to the Future 3
- Back to the Future 2

Prix total : 48
```

*Explication* :

((15*4)*0.8) = 48

**Exemple n°6**

```
Panier :

- Back to the Future 1
- Back to the Future 2
- Back to the Future 3
- La chèvre

Prix total : 56
```

*Explication :*

((15*3)*0.8)+20 = 56

## Get started

### Install dependencies

```shell
  npm install
```

### Run the tests

```shell
  npm run test
```

### Run the project

```shell
  npm run start
```
