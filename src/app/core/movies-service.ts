import { Movie } from './movie';

export class MoviesService {
  #availableMovies: Movie[] = [
    {
      id: '1',
      title: 'Back to the future',
      saga: 'Back to the future',
    },
    {
      id: '2',
      title: 'Back to the future 2',
      saga: 'Back to the future',
    },
    {
      id: '3',
      title: 'Back to the future 3',
      saga: 'Back to the future',
    },
    {
      id: '4',
      title: 'Star Wars: Episode IV - A New Hope',
      saga: 'Star Wars',
    },
    {
      id: '5',
      title: 'Star Wars: Episode V - The Empire Strikes Back',
      saga: 'Star Wars',
    },
    {
      id: '6',
      title: 'Star Wars: Episode VI - Return of the Jedi',
      saga: 'Star Wars',
    },
    {
      id: '7',
      title: 'The Goonies',
      saga: null,
    },
    {
      id: '8',
      title: 'The Karate Kid',
      saga: null,
    },
    {
      id: '9',
      title: 'Men in Black',
      saga: null,
    },
    {
      id: '10',
      title: 'The Lion King',
      saga: null,
    },
  ];

  public async getAvailableMovies(): Promise<Movie[]> {
    return this.#availableMovies;
  }
}
