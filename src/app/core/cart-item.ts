import { Movie } from './movie';

export interface CartItem {
  movie: Movie;
  quantity: number;
}
