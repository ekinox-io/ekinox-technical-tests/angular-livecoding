import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CartItem } from './cart-item';
import { MoviesService } from './movies-service';

@Injectable()
export class CartService {
  private items: BehaviorSubject<CartItem[]>;

  public constructor(private readonly moviesService: MoviesService) {
    this.items = new BehaviorSubject<CartItem[]>([]);
    this.refreshCart().catch(e => {
      throw new Error('Error while fetching books', e);
    });
  }

  /**
   *
   * @returns An observable of the cart items
   */
  public getCartItems(): Observable<CartItem[]> {
    return this.items.asObservable();
  }

  /**
   * Increment the quantity of a movie in the cart
   */
  public incrementQuantity(movieId: string) {
    const selectedMovies = this.items.getValue();
    const movieIndex = selectedMovies.findIndex(selectedMovie => selectedMovie.movie.id === movieId);
    if (movieIndex === -1) {
      throw new Error(`Movie with id ${movieId} not found in selection`);
    }

    const newItem = {
      ...selectedMovies[movieIndex],
      quantity: selectedMovies[movieIndex].quantity + 1,
    };

    this.items.next(selectedMovies.toSpliced(movieIndex, 1, newItem));
  }

  /**
   * Decrement the quantity of a movie in the cart
   */
  public decrementQuantity(movieId: string) {
    const selectedMovies = this.items.getValue();
    const movieIndex = selectedMovies.findIndex(selectedMovie => selectedMovie.movie.id === movieId);
    if (movieIndex === -1) {
      throw new Error(`Movie with id ${movieId} not found in selection`);
    }

    const newQuantity = Math.max(0, selectedMovies[movieIndex].quantity - 1);

    const newItem = {
      ...selectedMovies[movieIndex],
      quantity: newQuantity,
    };

    this.items.next(selectedMovies.toSpliced(movieIndex, 1, newItem));
  }

  private async refreshCart() {
    const availableMovies = await this.moviesService.getAvailableMovies();
    this.items.next(availableMovies.map(movie => ({ quantity: 0, movie })));
  }
}
