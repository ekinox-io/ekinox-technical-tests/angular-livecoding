export interface Movie {
  id: string;
  title: string;
  saga: 'Back to the future' | 'Star Wars' | null;
}
