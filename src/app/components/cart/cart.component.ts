import { Component } from '@angular/core';
import { CartService } from '../../core/cart-service';
import { CartItemComponent } from '../cart-item/cart-item.component';
import { MoviesService } from '../../core/movies-service';
import { CartItem } from '../../core/cart-item';

@Component({
  selector: 'app-cart',
  standalone: true,
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss',
  providers: [CartService, MoviesService],
  imports: [CartItemComponent],
})
export class CartComponent {
  public sampleItem: CartItem = { movie: { id: '1', title: 'star wars episode 1', saga: 'Star Wars' }, quantity: 1 };

  public constructor(private cartService: CartService) {}

  public increment(id: string) {
    this.cartService.incrementQuantity(id);
  }

  public decrement(id: string) {
    this.cartService.decrementQuantity(id);
  }
}
