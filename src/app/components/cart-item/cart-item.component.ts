import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CartItem } from '../../core/cart-item';

@Component({
  selector: '[app-cart-item]',
  standalone: true,
  templateUrl: './cart-item.component.html',
  styleUrl: './cart-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartItemComponent {
  @Input({ required: true }) public cartItem!: CartItem;
  @Output() public incrementQuantity = new EventEmitter<void>();
  @Output() public decrementQuantity = new EventEmitter<void>();

  public increment(): void {
    this.incrementQuantity.emit();
  }

  public decrement(): void {
    this.decrementQuantity.emit();
  }
}
